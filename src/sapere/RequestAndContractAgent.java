package sapere;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Timer;
import java.util.TimerTask;
import eu.sapere.middleware.agent.SapereAgent;
import eu.sapere.middleware.lsa.Property;
import eu.sapere.middleware.lsa.SyntheticPropertyName;
import eu.sapere.middleware.node.NodeManager;
import eu.sapere.middleware.node.notifier.event.BondEvent;
import eu.sapere.middleware.node.notifier.event.DecayedEvent;
import eu.sapere.middleware.node.notifier.event.LsaUpdatedEvent;
import eu.sapere.middleware.node.notifier.event.PropagationEvent;
import eu.sapere.middleware.node.notifier.event.RewardEvent;

public class RequestAndContractAgent extends SapereAgent {

	private static final long serialVersionUID = -6664396090994936585L;
	private static final double THRESHOLD = 10;
	public static Map<String, Payload> proposals;
	public static String contactWith = "";

	public RequestAndContractAgent(String agentName) {
		super(NodeManager.nodeName + ":" + agentName, new String[] {""},new String[] {""}, "");
		proposals = new HashMap<>();
	}

	@Override
	public void setInitialLSA() {
		Payload payload = new Payload(1, true, LocalSettingsAgent.quantity, "green");
		Property prop = new Property("Light", payload);
		lsa.addProperty(prop);
		lsa.addSubDescription(new String[] { "E" });
		lsa.addSyntheticProperty(SyntheticPropertyName.OUTPUT, "E");
		lsa.addSyntheticProperty(SyntheticPropertyName.TYPE, "R");
		lsa.addSyntheticProperty(SyntheticPropertyName.QUERY, this.agentName);
		new Timer().schedule(new TimerTask() {
			@Override
			public void run() {
				try {
					if (LocalSettingsAgent.quantity < THRESHOLD && !LocalSettingsAgent.contract
							&& proposals.isEmpty()) {
						lsa.addSyntheticProperty(SyntheticPropertyName.TYPE, "R");
						lsa.addSyntheticProperty(SyntheticPropertyName.DECAY, "2");
						if (lsa.hasSyntheticProperty(SyntheticPropertyName.DESTINATION))
							lsa.removeSyntheticProperty(SyntheticPropertyName.DESTINATION);
						System.out.println("Sending request");
						addGradient(1);
					}
					if ((LocalSettingsAgent.luminance > 50 && LocalSettingsAgent.sending == 0
							&& LocalSettingsAgent.contract)
							|| (LocalSettingsAgent.quantity < THRESHOLD && LocalSettingsAgent.contract)) {
						LocalSettingsAgent.receiving = 0;
						LocalSettingsAgent.sending = 0;
						LocalSettingsAgent.contract = false;
						lsa.addSyntheticProperty(SyntheticPropertyName.TYPE, "D");
						lsa.addSyntheticProperty(SyntheticPropertyName.DECAY, "2");
						System.out.println("sending remove contract to :" + contactWith);
						proposals.clear();
						sendTo(lsa,contactWith);
					}
				} catch (Exception e) {
					System.err.println(e.toString());
				}
			}
		}, 0, 5000);
		this.submitOperation();
	}

	@Override
	public void onBondNotification(BondEvent event) {
		if (event.getBondedLsa().getSyntheticProperty(SyntheticPropertyName.TYPE).toString().equals("P") && !event
				.getBondedLsa().getSyntheticProperty(SyntheticPropertyName.SOURCE).equals(NodeManager.localIP)) {
			System.out.println("Propsal received from " + event.getBondedLsa().getAgentName());
			System.out.println("Propsal:" + event.getBondedLsa().getProperties().get(0).toString());
			proposals.put(event.getBondedLsa().getSyntheticProperty(SyntheticPropertyName.SOURCE).toString(),
					(Payload) event.getBondedLsa().getProperties().get(0).getValue());

		}

		// compareAndSelect(proposals);
		// sendContracts();
	}

	private String compareProposals(Map<String, Payload> proposals) {
		String ip = "";
		double lowestPrice = 10;
		for (Entry<String, Payload> prop : proposals.entrySet()) {
			if (prop.getValue().getPrice() < lowestPrice) {
				ip = prop.getKey();
				lowestPrice = prop.getValue().getPrice();
			}
		}
		return ip;
	}

	@Override
	public void onPropagationEvent(PropagationEvent event) {
	}

	@Override
	public void onDecayedNotification(DecayedEvent event) {
		if (lsa.getSyntheticProperty(SyntheticPropertyName.DECAY).equals("1") && !LocalSettingsAgent.contract
				&& !proposals.isEmpty()) {
			contactWith = compareProposals(proposals);
			System.out.println("Sending contract to:" + contactWith);
			lsa.addSyntheticProperty(SyntheticPropertyName.TYPE, "C");
			lsa.addSyntheticProperty(SyntheticPropertyName.DECAY, "2");
			this.sendTo(lsa,contactWith);
			LocalSettingsAgent.quantity = 30;
			LocalSettingsAgent.receiving = 30;
			LocalSettingsAgent.contract = true;
			proposals.clear();
		}
	}

	@Override
	public void onLsaUpdatedEvent(LsaUpdatedEvent event) {
	}

	@Override
	public void onRewardEvent(RewardEvent event) {

	}

}
