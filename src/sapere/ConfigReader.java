package sapere;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;

/**
 * Configuration
 */
public class ConfigReader {

	private String nodeName;
	private String localIp;
	public static double price;
	private String[] data;
	private String[] neighs;
	/**
	 * Config path
	 */
	public static final String CONFIG_FILE = "config.txt";

	/**
	 * Network configuration
	 */
	public ConfigReader() {
		try {
			InputStream fis = new FileInputStream(new File(CONFIG_FILE));
			BufferedReader reader = new BufferedReader(new InputStreamReader(fis));
			data = reader.readLine().replaceAll("\\s+", "").split(",");
			nodeName = data[0];
			localIp = data[1];
			price = Double.parseDouble(data[2]);
			neighs = Arrays.copyOfRange(data, 3, data.length);
			reader.close();
			fis.close();
		} catch (Exception ex) {
			System.out.println(ex.toString());
		}
	}

	public String getNodeName() {
		return nodeName;
	}

	public String getLocalIp() {
		return localIp;
	}

	public String[] getNeighs() {
		return neighs;
	}
}
