package sapere;

import java.io.Serializable;

public class Payload implements Serializable{

	private static final long serialVersionUID = -6633559839266065420L;
	private double price;
	private Boolean availability;
	private double quantity;
	private String type;

	public Payload(double price, Boolean availability, double quantity, String type) {
		this.price = price;
		this.quantity = quantity;
		this.availability = availability;
		this.type = type;
	}

	@Override
	public String toString() {
		return  price + "," + quantity;
	}

	public Boolean getAvailability() {
		return availability;
	}

	public void setAvailability(Boolean availability) {
		this.availability = availability;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public double getQuantity() {
		return quantity;
	}

	public void setQuantity(double quantity) {
		this.quantity = quantity;
	}

}
