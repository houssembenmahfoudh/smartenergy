package sapere;

import eu.sapere.middleware.node.NodeManager;

public class SapereStarter {


	public static void main(String[] args) {
		System.out.println("SAPERE is starting...");
		System.out.println("SAPERE is starting...");
		ConfigReader config = new ConfigReader();
		NodeManager.instance().getNetworkDeliveryManager().setNeighbours(config.getNeighs());
		NodeManager.localIP = config.getLocalIp();
		NodeManager.nodeName = config.getNodeName();

		LocalSettingsAgent localAgentSetting = new LocalSettingsAgent("localAgentSetting");
		localAgentSetting.setInitialLSA();
		RequestAndContractAgent requestAndContractAgent = new RequestAndContractAgent("requestAndContractAgent");
		requestAndContractAgent.setInitialLSA();
		ProposalEnergyAgent proposalEnergyAgent = new ProposalEnergyAgent("proposalEnergyAgent");
		proposalEnergyAgent.setInitialLSA();
	}
}
