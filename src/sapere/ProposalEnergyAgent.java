package sapere;

import eu.sapere.middleware.agent.SapereAgent;
import eu.sapere.middleware.lsa.Property;
import eu.sapere.middleware.lsa.SyntheticPropertyName;
import eu.sapere.middleware.node.NodeManager;
import eu.sapere.middleware.node.notifier.event.BondEvent;
import eu.sapere.middleware.node.notifier.event.DecayedEvent;
import eu.sapere.middleware.node.notifier.event.LsaUpdatedEvent;
import eu.sapere.middleware.node.notifier.event.PropagationEvent;
import eu.sapere.middleware.node.notifier.event.RewardEvent;

public class ProposalEnergyAgent extends SapereAgent {

	private static final long serialVersionUID = -6664396090994936585L;
	private Boolean proposalActive = false;

	public ProposalEnergyAgent(String agentName) {
		super(NodeManager.nodeName + ":" + agentName, new String[] { "" }, new String[] { "" }, "");
	}

	@Override
	public void setInitialLSA() {
		lsa.addSubDescription(new String[] { "E" });
		lsa.addSyntheticProperty(SyntheticPropertyName.OUTPUT, "E");
		Payload payload = new Payload(ConfigReader.price, true, 70, "green");
		Property prop = new Property("Light", payload);
		lsa.addProperty(prop);
		lsa.addSyntheticProperty(SyntheticPropertyName.TYPE, "P");
		this.submitOperation();
	}

	@Override
	public void onBondNotification(BondEvent event) {
		if (event.getBondedLsa().getSyntheticProperty(SyntheticPropertyName.TYPE).equals("R")
				&& LocalSettingsAgent.quantity > 20 && !LocalSettingsAgent.contract
				&& !event.getBondedLsa().getSyntheticProperty(SyntheticPropertyName.SOURCE).equals(NodeManager.localIP)
				&& !proposalActive) {
			System.out.println("Request received and proposal sent");
			lsa.addSyntheticProperty(SyntheticPropertyName.QUERY, event.getBondedLsa().getAgentName());
			lsa.addSyntheticProperty(SyntheticPropertyName.DECAY, "3");
			proposalActive = true;
			this.sendTo(lsa, event.getBondedLsa().getSyntheticProperty(SyntheticPropertyName.SOURCE).toString());
		}
		// if detectRequest() and satisfyRequest() then sendProposal();
		// allocateEnergy(time);
		if (event.getBondedLsa().getSyntheticProperty(SyntheticPropertyName.TYPE).equals("C")
				&& !LocalSettingsAgent.contract && !event.getBondedLsa()
						.getSyntheticProperty(SyntheticPropertyName.SOURCE).equals(NodeManager.localIP)) {
			LocalSettingsAgent.sending = LocalSettingsAgent.luminance / 2;
			LocalSettingsAgent.contract = true;
			System.out.println("Contracting with:" + event.getBondedLsa().getAgentName());
			RequestAndContractAgent.contactWith = event.getBondedLsa()
					.getSyntheticProperty(SyntheticPropertyName.SOURCE).toString();
		}

		if (event.getBondedLsa().getSyntheticProperty(SyntheticPropertyName.TYPE).equals("D") && !event.getBondedLsa()
				.getSyntheticProperty(SyntheticPropertyName.SOURCE).equals(NodeManager.localIP)) {
			System.out.println("deleting contract with " + event.getBondedLsa().getAgentName());
			LocalSettingsAgent.sending = 0;
			LocalSettingsAgent.receiving = 0;
			LocalSettingsAgent.contract = false;
			proposalActive = false;
			RequestAndContractAgent.proposals.clear();
			RequestAndContractAgent.contactWith = "";
		}

		// if receiveContract() then
		// sendEnergy();
		// update(payload);

	}

	@Override
	public void onPropagationEvent(PropagationEvent event) {
	}

	@Override
	public void onDecayedNotification(DecayedEvent event) {
		if (lsa.getSyntheticProperty(SyntheticPropertyName.DECAY).equals("0")) {
			proposalActive = false;
		}
	}

	@Override
	public void onLsaUpdatedEvent(LsaUpdatedEvent event) {
	}

	@Override
	public void onRewardEvent(RewardEvent event) {

	}

}
