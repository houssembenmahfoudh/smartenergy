package sapere;

import java.util.Timer;
import java.util.TimerTask;
import eu.sapere.middleware.agent.SapereAgent;
import eu.sapere.middleware.lsa.Property;
import eu.sapere.middleware.lsa.SyntheticPropertyName;
import eu.sapere.middleware.node.NodeManager;
import eu.sapere.middleware.node.notifier.event.BondEvent;
import eu.sapere.middleware.node.notifier.event.DecayedEvent;
import eu.sapere.middleware.node.notifier.event.LsaUpdatedEvent;
import eu.sapere.middleware.node.notifier.event.PropagationEvent;
import eu.sapere.middleware.node.notifier.event.RewardEvent;
import utils.BulbActuator;
import utils.Sensors;

public class LocalSettingsAgent extends SapereAgent {

	private static final long serialVersionUID = 5920535286218204783L;
	public static double luminance = 100;
	public static double sending = 0;
	public static double receiving = 0;
	public static double quantity = 100;
	public static Boolean contract = false;

	public LocalSettingsAgent(String agentName) {
		super(NodeManager.nodeName + ":" + agentName, new String[] {""}, new String[] {""}, "");
	}

	@Override
	public void setInitialLSA() {
		Payload payload = new Payload(ConfigReader.price, true, luminance, "green");
		Property prop = new Property("Light", payload);
		lsa.addSyntheticProperty(SyntheticPropertyName.TYPE, "S");
		new Timer().schedule(new TimerTask() {
			@Override
			public void run() {
				try {
					luminance = Sensors.sendingGetRequest("get_luminance");
					payload.setQuantity(luminance);
					prop.setValue(payload);
					lsa.addProperty(prop);
					BulbActuator.sendingPostRequest(luminance, sending, receiving);
					quantity = luminance - sending + receiving;
					System.out.println("Price " + ConfigReader.price + " Quantity " + quantity + " sending " + sending
							+ " receiving " + receiving + " luminance " + luminance + " Contracting "
							+ RequestAndContractAgent.contactWith);

				} catch (Exception e) {
					System.out.println(e.toString());
				}
			}
		}, 0, 5000);
		this.submitOperation();
	}

	@Override
	public void onPropagationEvent(PropagationEvent event) {
		System.out.println("onPropagationEvent:" + agentName);
	}

	@Override
	public void onDecayedNotification(DecayedEvent event) {
		System.out.println("** onDecayedNotification ** " + agentName);
	}

	@Override
	public void onLsaUpdatedEvent(LsaUpdatedEvent event) {
		System.out.println("onLsaUpdatedEvent:" + agentName);
	}

	@Override
	public void onBondNotification(BondEvent event) {
		System.out.println("** LocalSettingsAgent ** " + agentName);
	}

	@Override
	public void onRewardEvent(RewardEvent event) {

	}

}
