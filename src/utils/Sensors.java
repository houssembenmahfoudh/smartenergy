package utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class Sensors {

	public static double sendingGetRequest(String action) {
		String urlString = "http://localhost:3671/sensors/2/" + action;
		URL url;
		StringBuffer response = null;
		double luminance = -1.0;
		try {
			url = new URL(urlString);

			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("GET");
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String output;
			response = new StringBuffer();

			while ((output = in.readLine()) != null) {
				if (output.contains("value"))
					response.append(output.substring(10));
			}
			luminance = Double.parseDouble(response.toString());
			in.close();
		} catch (Exception e) {
			 e.printStackTrace();
		}
		if (luminance > 100)
			luminance = 100;
		return luminance;
	}
}
