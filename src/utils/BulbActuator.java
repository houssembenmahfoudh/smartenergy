package utils;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class BulbActuator {

	public static void sendingPostRequest(double luminance, double sending, double receiving) {

		String url = "http://localhost:3671/dimmers/set_level";
		URL obj;
		double light = 0.0;
		try {
			light = luminance - sending + receiving;
			if (light < 10)
				light = 0;
			else if (light > 60)
				light = 100;
			obj = new URL(url);
			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			con.setRequestMethod("POST");
			con.setRequestProperty("Content-Type", "application/json");
			String postJsonData = "{\"node_id\":\"3\",\"value\":" + light + "}";
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			wr.writeBytes(postJsonData);
			wr.flush();
			wr.close();
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
			String output;
			StringBuffer response = new StringBuffer();
			while ((output = in.readLine()) != null) {
				response.append(output);
			}
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static boolean isInteger(String str) {
		return str.matches("^-?\\d+$");
	}

}
